A simple application that display a randow picture using Unsplash API.

## Run with Docker

```
docker run -p 5000:5000 -e UNSPLASH_ACCESS_KEY=$UNSPLASH_ACCESS_KEY techwhaleio/random-picts
```

## Run on Kubernetes

```
kubectl apply -f ./deploy
```
