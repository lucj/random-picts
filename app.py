from flask import Flask, render_template
import logging
import os
import requests
import socket
import sys

# Define log level
logging.basicConfig(level=logging.DEBUG)

# Define application
app = Flask(__name__)

# Get Unsplash access key
UNSPLASH_ACCESS_KEY = os.getenv('UNSPLASH_ACCESS_KEY')

# Make sure access key is provided
if(UNSPLASH_ACCESS_KEY == None) or (UNSPLASH_ACCESS_KEY == ""):
  app.logger.error("Unsplash API key must be provided in the UNSPLASH_ACCESS_KEY env variable")
  sys.exit(0)

# Photos filtering
UNSPLASH_QUERY= os.getenv('UNSPLASH_QUERY', 'insect-worms')

# Get random image
def get_photo():
  # Call Unsplash API
  url = "https://api.unsplash.com/photos/random?query={}&client_id={}".format(UNSPLASH_QUERY, UNSPLASH_ACCESS_KEY)
  r = requests.get(url)

  # Make sure api limit is not reached
  if r.status_code != 200:
    app.logger.error("cannot get photo from Unsplash: {}".format(r.status_code))
    return { "url": "", "user": "Andy Holmes" }
  
  # Get JSON result
  json = r.json()

  # Get photo information
  url = json["urls"]["regular"]
  user = json["user"]["name"]
  app.logger.info("URL:{}, USER:{}".format(url, user))
  return {"url": url, "user": user}

@app.route('/')
def hello():
    data = get_photo()
    return render_template('index.html',
                           url=data["url"],
                           user=data["user"],
                           host=socket.gethostname())

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
